BUILD_DIR=build
SRC_DIR=src
TB_DIR=tb
PRJ_DIR=quartus
PRJ_NAME=ambi_fpga

bitstream: ${BUILD_DIR}
	quartus_sh --flow compile ${PRJ_DIR}/${PRJ_NAME}

${BUILD_DIR}:
	mkdir -p ${BUILD_DIR}

clean:
	rm -rf build

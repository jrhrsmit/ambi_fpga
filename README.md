# ambi_fpga
Custom ambilight implementation using a TVP7002 VGA DAC for 1080p 60Hz
displays.

## Hardware
To make the HDMI signal more easy to parse, an external HDMI to VGA converter
is used (2$ Aliexpress).
The VGA signal is fed into the TVP7002, which uses three standalone DACs to
convert the signal to 3x9-bits RGB.
This setup requires little hardware inside the FPGA for decoding the video
signal and no understanding of HDMI, which makes it a simple implementation and
keeps the focus on implementing the ambilight algorithm in hardware.
The downside of this implementation is that it only supports up to 1080p60
because of VGA, and it uses at least two external chips (the TVP7002 and the
HDMI to VGA converter).
However, the offloading of HDMI decoding relaxes the speed requirements for the
Cyclone IV FPGA, reducing the overall cost of the project.

```
+---------------+         +---------------+         +-----------+              +--------------+         +------------+
|               |  HDMI   |               |   VGA   |           |  27-bit RGB  |              |  SPI    |            |
|  HDMI Source  +-------->+  HDMI to VGA  +-------->+  TVP7002  +------------->+  Cyclone IV  +-------->+   SK9822   |
|               |         |   converter   |         |  VGA DAC  |  H/V-Sync    |     FPGA     |         |  LED strip |
|               |         |               |         |           +------------->+              |         |            |
+---------------+         +---------------+         +-----------+              +--------------+         +------------+
```

The devboard used is the A-C4E6E10 (also Aliexpress) with a Cyclone IV
EP4CE6E22C8N.
I believe the devboard is not in production anymore, but I also wouldn't
recommend it if it was due to the low amount of exposed pins.

One other important feature is that the TVP7002 is configured to sample on its
own pixel clock of 27MHz instead of the normal FHD pixel clock of 148MHz.
This actually reduces the size of frame from 1920x1080 to about 350x1080 (only
the horizontal dimension gets compressed).
This again relaxes the timing requirements for the FPGA and leaves enough
sampled pixels per LED to obtain a representative average.

## RTL Architecture
The top-level entity is `ambi_fpga` inside `ambi_fpga.vhd`.

The datapath is pipelined into the following stages:
 - Input bufffers
   - `input_buf_inst : input_buf`
   - `input_ff_inst : input_buffer`
 - `led_pointer_inst : led_pointer`
   Calculates the X and Y coordinates in LED units for the current pixel
 - `vga_inst : vga`
   Averages the pixel data and calculates the position in memory for
   rgb_buffer
 - `lut_inst : lut`
   A lookup table to do colour correction to better match the colours of the
   LEDs to the TV, currently only contains a simple gamma correction (which
   is pretty accurate).
 - `rgb_buffer_inst : rgb_buffer`
   M9K memory which contains the values for the pixels for the current frame
 - `spi_inst : spi`
   SPI instance specific for the LED strip and the rgb_buffer
 - `i2c_inst : i2c`
   I2C for initializing the TVP7002 with the correct settings
 - Output buffers
   - `i2c_io_buf_inst : i2c_io_buf`
   - `spi_io_buf_inst : spi_io_buf`
 - `seven_segment_controller_inst : seven_segment_controller`
   Driver for the seven segment display used for debugging and feedback,
   accepts a 32-bit unsigned int and prints it in hex to the 8-digit
   7-segment display.
 - `tvp7002_control_inst : tvp7002_control`
   Controls the pin reset for the TVP7002
 - `ambi_pll2_inst : ambi_pll2`
   PLL for clock generation, generates 10kHz for seven segment controller
   and TVP7002 reset controller, 400kHz for I2C controller, and 27MHz for
   the pixel data clock, at which the rest of the system runs.

A PDF of the RTL viewer can be found in the `doc/` folder.

## Resources
The design uses about 4923 using balanced optimization in Quartus, and 3k
memory bits.
This design fits in the Cyclone IV EP4CE6 with 6k LEs (which is used on the devboard).

## Pinout
The pinout is a little bit of a mess due to the use of a devboard for the
Cyclone IV, and the low-effort prototype of the TVP7002 attachment.
The hardware is also not (yet) on Gitlab, because it is pretty much useless, so
this is just for my personal reference.

LCDVCC is selected with J1 (set it to 3.3V)

```
Pin	Devboard					Description			Wire colour
1	GND							GND to LEDs			black
2	LCDVCC					
3	10k pot with GND or 18	
4	PIN_85						EXT_CLK, clk27mhz	blue
5	PIN_99						vsync				yellow
6	PIN_100						data_enable			brown
7	PIN_101						SCL					yellow
8	PIN_103						SDA					green
9	PIN_104						BLUE3				green
10	PIN_105						BLUE4				blue
11	PIN_106						RESETB				yellow
12	PIN_110						data_enable_syn		
13	PIN_111						SCK					yellow
14	PIN_112						MOSI				green
15	LCDVCC
16	GND
17	4k7 pullup to LCDVCC
18	10k pot with 3 or NC
19	LCDVCC
20	LCDVCC
```

## Future plans
 - Support 4k (would require complete redesign as VGA wouldn't be possible and
   the FPGA is too slow)
 - Design custom PCB which contains all necessary hardware to translate from
   HDMI to whatever protocol is used on the input of the FPGA, so no external
   dongles are required.
 - Make a nice box that contains everything

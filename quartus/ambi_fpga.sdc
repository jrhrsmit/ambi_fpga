# inform quartus that the clk port brings a 50MHz clock into our design so
	# that timing closure on our design can be analyzed

# create_clock -name clk50mhz -period "50MHz" [get_ports clk50mhz]
derive_pll_clocks -create_base_clocks
derive_clock_uncertainty


# inform quartus that the LED output port has no critical timing requirements
	# its a single output port driving an LED, there are no timing relationships
	# that are critical for this

# set_false_path -from * -to [get_ports hb]
#include <stdio.h>
#include <stdint.h>
#include <math.h>

struct RGB
{
	unsigned char R;
	unsigned char G;
	unsigned char B;
};

struct HSV
{
	double H;
	double S;
	double V;
};
struct RGB HSVToRGB(struct HSV hsv);

int
main ()
{
  struct HSV hsvdata = {0};
  struct RGB rgbdata;
  printf ("signal buf : rgbbuf := (\n");
  int numleds = 17*2+31*2;
  int i;
  printf(
  	"DEPTH = 128;\n"
  	"WIDTH = 24;\n"
  	"ADDRESS_RADIX = HEX;\n"
  	"DATA_RADIX = HEX;\n"
  	"CONTENT\n"
  	"BEGIN\n");
  for (i = 0; i < numleds; i++)
    {
        hsvdata.H += 8;
        if(hsvdata.H > 360)
            hsvdata.H -= 360;
        hsvdata.S = 1;
        hsvdata.V = 1;
        rgbdata = HSVToRGB(hsvdata);
      // printf ("x\"00%02X%02X%02X\"%c\n", rgbdata.R, rgbdata.G, rgbdata.B, i == numleds -1 ? ' ':',');
        printf("%X\t:\t00%02X%02X%02X;\n", i, rgbdata.R, rgbdata.G, rgbdata.B);
    }
    printf("[%X..%X]\t:\t00000000;\n", i, 127);
    printf("END;\n");

  return 0;
}
/*****Please include following header files*****/
// math.h
/***********************************************/


struct RGB HSVToRGB(struct HSV hsv) {
	double r = 0, g = 0, b = 0;

	if (hsv.S == 0)
	{
		r = hsv.V;
		g = hsv.V;
		b = hsv.V;
	}
	else
	{
		int i;
		double f, p, q, t;

		if (hsv.H == 360)
			hsv.H = 0;
		else
			hsv.H = hsv.H / 60;

		i = (int)trunc(hsv.H);
		f = hsv.H - i;

		p = hsv.V * (1.0 - hsv.S);
		q = hsv.V * (1.0 - (hsv.S * f));
		t = hsv.V * (1.0 - (hsv.S * (1.0 - f)));

		switch (i)
		{
		case 0:
			r = hsv.V;
			g = t;
			b = p;
			break;

		case 1:
			r = q;
			g = hsv.V;
			b = p;
			break;

		case 2:
			r = p;
			g = hsv.V;
			b = t;
			break;

		case 3:
			r = p;
			g = q;
			b = hsv.V;
			break;

		case 4:
			r = t;
			g = p;
			b = hsv.V;
			break;

		default:
			r = hsv.V;
			g = p;
			b = q;
			break;
		}

	}

	struct RGB rgb;
	rgb.R = r * 255;
	rgb.G = g * 255;
	rgb.B = b * 255;

	return rgb;
}
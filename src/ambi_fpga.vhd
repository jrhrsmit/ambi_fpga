library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity ambi_fpga is
    generic(
               LEDS_H_MAX_IDX : integer := 30;
               LEDS_V_MAX_IDX : integer := 16;
               LINE_MAX_IDX   : integer := 1079;
               PX_MAX_IDX     : integer := 348
           );
    port(
            inclk50mhz          : in  std_logic;
            rst                 : in  std_logic;
            ss1                 : out std_logic;
            ss2                 : out std_logic;
            ss3                 : out std_logic;
            ss4                 : out std_logic;
            ss5                 : out std_logic;
            ss6                 : out std_logic;
            ss7                 : out std_logic;
            ss8                 : out std_logic;
            ssa                 : out std_logic;
            ssb                 : out std_logic;
            ssc                 : out std_logic;
            ssd                 : out std_logic;
            sse                 : out std_logic;
            ssf                 : out std_logic;
            ssg                 : out std_logic;
            ssdp                : out std_logic;
            sck_buf             : out std_logic;
            mosi_buf            : out std_logic;
            vsync_raw           : in  std_logic;
            hsync_raw           : in  std_logic;
            dip_sw              : in  std_logic_vector(7 downto 0);
            led3                : out std_logic;
            led4                : out std_logic;
            led5                : out std_logic;
            led6                : out std_logic;
            led7                : out std_logic;
            led8                : out std_logic;
            resetb              : out std_logic;
            data_enable_raw     : in  std_logic;
            ext_clk             : out std_logic;
            scl_buf             : out std_logic;
            sda_buf             : out std_logic;
            dataclk_raw         : in  std_logic;
            r_raw               : in  std_logic_vector(7 downto 1);
            g_raw               : in  std_logic_vector(7 downto 1);
            b_raw               : in  std_logic_vector(7 downto 1);
            data_enable_syn_out : out std_logic
        );
end entity ambi_fpga;

architecture behavioural of ambi_fpga is
    signal clk10khz           : std_logic;
    signal clk400khz          : std_logic;
    signal clk27mhz           : std_logic;
    signal ss                 : std_logic_vector(31 downto 0);
    signal dest_get           : unsigned(7 downto 0);
    signal data8_r            : unsigned(7 downto 0) := (others => '0');
    signal data8_g            : unsigned(7 downto 0) := (others => '0');
    signal data8_b            : unsigned(7 downto 0) := (others => '0');
    signal dest_set           : unsigned(7 downto 0);
    signal dest_set_delay     : unsigned(7 downto 0);
    signal data8_r_get        : unsigned(7 downto 0) := (others => '0');
    signal data8_g_get        : unsigned(7 downto 0) := (others => '0');
    signal data8_b_get        : unsigned(7 downto 0) := (others => '0');
    signal spi_buf_empty      : std_logic;
    signal r                  : unsigned(7 downto 0);
    signal g                  : unsigned(7 downto 0);
    signal b                  : unsigned(7 downto 0);
    signal r2                 : unsigned(7 downto 0);
    signal g2                 : unsigned(7 downto 0);
    signal b2                 : unsigned(7 downto 0);
    signal r_sum              : unsigned(17 downto 0);
    signal g_sum              : unsigned(17 downto 0);
    signal b_sum              : unsigned(17 downto 0);
    signal r_avg              : std_logic_vector(17 downto 0);
    signal g_avg              : std_logic_vector(17 downto 0);
    signal b_avg              : std_logic_vector(17 downto 0);
    signal num_samples        : unsigned(9 downto 0);
    signal hsync              : std_logic;
    signal vsync              : std_logic;
    signal hsync_buf          : std_logic;
    signal vsync_buf          : std_logic;
    signal input_buf_datain   : std_logic_vector(26 downto 0);
    signal input_buf_dataout  : std_logic_vector(4 downto 0);
    signal i2c_iobuf0_datain  : std_logic_vector(1 downto 0);
    signal i2c_iobuf0_dataout : std_logic_vector(1 downto 0);
    signal sda                : std_logic;
    signal scl                : std_logic;
    signal dataclk            : std_logic;
    signal data_enable        : std_logic;
    signal data_enable_buf    : std_logic;
    signal data_enable_syn    : std_logic;
    signal data_enable_syn2   : std_logic;
    signal r_buf              : std_logic_vector(7 downto 0);
    signal g_buf              : std_logic_vector(7 downto 0);
    signal b_buf              : std_logic_vector(7 downto 0);
    signal r_adj              : unsigned(7 downto 0);
    signal g_adj              : unsigned(7 downto 0);
    signal b_adj              : unsigned(7 downto 0);
    signal data_enable_adj    : std_logic;
    signal led_x              : integer range 0 to LEDS_H_MAX_IDX;
    signal led_y              : integer range 0 to LEDS_V_MAX_IDX;
    signal led_x_end          : std_logic;
    signal led_y_end          : std_logic;
    signal div_numer          : unsigned(17 downto 0);
    signal div_denom          : unsigned(17 downto 0);
    signal div_quotient       : unsigned(17 downto 0);
    signal sck                : std_logic            := '1';
    signal mosi               : std_logic            := '1';
    signal avg_r : unsigned(7 downto 0);
    signal avg_g : unsigned(7 downto 0);
    signal avg_b : unsigned(7 downto 0);
    signal dest_set_lut : unsigned(7 downto 0);

    component seven_segment_controller is
        port(
                clk      : in  std_logic;
                rst      : in  std_logic;
                input    : in  std_logic_vector(31 downto 0);
                ss_led1  : out std_logic;
                ss_led2  : out std_logic;
                ss_led3  : out std_logic;
                ss_led4  : out std_logic;
                ss_led5  : out std_logic;
                ss_led6  : out std_logic;
                ss_led7  : out std_logic;
                ss_led8  : out std_logic;
                ss_leda  : out std_logic;
                ss_ledb  : out std_logic;
                ss_ledc  : out std_logic;
                ss_ledd  : out std_logic;
                ss_lede  : out std_logic;
                ss_ledf  : out std_logic;
                ss_ledg  : out std_logic;
                ss_leddp : out std_logic
            );
    end component seven_segment_controller;

    component ambi_pll2 is
        port(
                areset : IN  STD_LOGIC := '0';
                inclk0 : IN  STD_LOGIC := '0';
                c0     : OUT STD_LOGIC;
                c1     : out std_logic;
                c2     : out std_logic
            );
    end component;

    component spi is
        port(
                clk            : in  std_logic;
                rst            : in  std_logic;
                sck            : out std_logic;
                mosi           : out std_logic;
                data8_r_get    : in  unsigned(7 downto 0);
                data8_g_get    : in  unsigned(7 downto 0);
                data8_b_get    : in  unsigned(7 downto 0);
                dest_get       : out unsigned(7 downto 0);
                start_transfer : in  std_logic;
                dip_sw         : in std_logic_vector(7 downto 0)
            );
    end component spi;

    component rgb_buffer is
        port(
                clk         : in  std_logic;
                rst         : in  std_logic;
                data8_r_set : in  unsigned(7 downto 0);
                data8_g_set : in  unsigned(7 downto 0);
                data8_b_set : in  unsigned(7 downto 0);
                dest_set    : in  unsigned(7 downto 0);
                data8_r_get : out unsigned(7 downto 0);
                data8_g_get : out unsigned(7 downto 0);
                data8_b_get : out unsigned(7 downto 0);
                dest_get    : in  unsigned(7 downto 0)
            );
    end component rgb_buffer;

    component vga is
        generic(
                   LEDS_H_MAX_IDX : integer := 30;
                   LEDS_V_MAX_IDX : integer := 16;
                   LINE_MAX_IDX   : integer := 1079;
                   PX_MAX_IDX     : integer := 348
               );
        port(
                clk            : in  std_logic;
                rst            : in  std_logic;
                vsync          : in  std_logic;
                data_enable    : in  std_logic;
                r              : in  unsigned(7 downto 0);
                g              : in  unsigned(7 downto 0);
                b              : in  unsigned(7 downto 0);
                led_x          : in  integer range 0 to LEDS_H_MAX_IDX;
                led_y          : in  integer range 0 to LEDS_V_MAX_IDX;
                led_y_end      : in  std_logic;
                r_avg          : out unsigned(7 downto 0);
                g_avg          : out unsigned(7 downto 0);
                b_avg          : out unsigned(7 downto 0);
                dest_set       : out unsigned(7 downto 0);
                dip_sw         : in  std_logic_vector(7 downto 0);
                ss_debug_out   : out std_logic_vector(31 downto 0)
            );
    end component vga;

    component input_buffer is
        port(
                clk             : in  std_logic;
                rst             : in  std_logic;
                r_buf           : in  std_logic_vector(7 downto 0);
                g_buf           : in  std_logic_vector(7 downto 0);
                b_buf           : in  std_logic_vector(7 downto 0);
                data_enable_buf : in  std_logic;
                hsync_buf       : in  std_logic;
                vsync_buf       : in  std_logic;
                r               : out unsigned(7 downto 0);
                g               : out unsigned(7 downto 0);
                b               : out unsigned(7 downto 0);
                hsync           : out std_logic;
                vsync           : out std_logic;
                data_enable     : out std_logic;
                data_enable_syn : out std_logic
            );
    end component input_buffer;

    component input_buf
        PORT(
                datain  : IN  STD_LOGIC_VECTOR(26 DOWNTO 0);
                dataout : OUT STD_LOGIC_VECTOR(26 DOWNTO 0)
            );
    end component;

    component i2c is
        port(
                rst     : in  std_logic;
                clk400k : in  std_logic;
                debug   : out std_logic;
                scl     : out std_logic;
                sda     : out std_logic
            );
    end component i2c;

    component i2c_io_buf IS
        PORT(
                datain  : IN  STD_LOGIC_VECTOR(1 DOWNTO 0);
                dataout : OUT STD_LOGIC_VECTOR(1 DOWNTO 0)
            );
    END component i2c_io_buf;

    component tvp7002_control is
        port(
                rst      : in  std_logic;
                clk10khz : in  std_logic;
                resetb   : out std_logic
            );
    end component tvp7002_control;

    component led_pointer is
        generic(
                   LEDS_H_MAX_IDX : integer := 30;
                   LEDS_V_MAX_IDX : integer := 16;
                   LINE_MAX_IDX   : integer := 1079;
                   PX_MAX_IDX     : integer := 348
               );
        port(
                clk             : in  std_logic;
                rst             : in  std_logic;
                vsync           : in  std_logic;
                data_enable     : in  std_logic;
                data_enable_out : out std_logic;
                r_in            : in  unsigned(7 downto 0);
                g_in            : in  unsigned(7 downto 0);
                b_in            : in  unsigned(7 downto 0);
                r_out           : out unsigned(7 downto 0);
                g_out           : out unsigned(7 downto 0);
                b_out           : out unsigned(7 downto 0);
                led_x           : out integer range 0 to LEDS_H_MAX_IDX;
                led_y           : out integer range 0 to LEDS_V_MAX_IDX;
                led_y_end       : out std_logic
            );
    end component led_pointer;

    component spi_io_buf
        PORT(
                datain  : IN  STD_LOGIC_VECTOR(1 DOWNTO 0);
                dataout : OUT STD_LOGIC_VECTOR(1 DOWNTO 0)
            );
    end component;

    component lut is
        port(
                clk         : in  std_logic;
                rst         : in  std_logic;
                avg_r       : in  unsigned(7 downto 0);
                avg_g       : in  unsigned(7 downto 0);
                avg_b       : in  unsigned(7 downto 0);
                dest_set    : in  unsigned(7 downto 0);
                data8_r     : out unsigned(7 downto 0);
                data8_g     : out unsigned(7 downto 0);
                data8_b     : out unsigned(7 downto 0);
                dest_set_lut : out unsigned(7 downto 0)
            );
    end component;

begin

    i2c_iobuf0_datain(0) <= scl;
    i2c_iobuf0_datain(1) <= sda;
    scl_buf              <= i2c_iobuf0_dataout(0);
    sda_buf              <= i2c_iobuf0_dataout(1);
    ext_clk              <= clk27mhz;
    led3                 <= '1';
    led4                 <= '1';
    led5                 <= '1';
    led6                 <= '1';
    led7                 <= '1';
    led8                 <= '1';
    data_enable_syn_out  <= data_enable_syn2;

    ambi_pll2_inst : ambi_pll2
    PORT MAP(
                inclk0 => inclk50mhz,
                c0     => clk400khz,
                c1     => clk27mhz,
                c2     => clk10khz
            );

    vga_inst : vga
    port map(
                clk            => clk27mhz,
                rst            => rst,
                vsync          => vsync,
                data_enable    => data_enable_syn2,
                r              => r2,
                g              => g2,
                b              => b2,
                led_x          => led_x,
                led_y          => led_y,
                led_y_end      => led_y_end,
                dip_sw         => not dip_sw,
                r_avg          => avg_r,
                g_avg          => avg_g,
                b_avg          => avg_b,
                dest_set       => dest_set,
                ss_debug_out => ss
            );

    led_pointer_inst : led_pointer
    port map(
                clk             => clk27mhz,
                rst             => rst,
                vsync           => vsync,
                data_enable     => data_enable_syn,
                data_enable_out => data_enable_syn2,
                led_x           => led_x,
                led_y           => led_y,
                led_y_end       => led_y_end,
                r_in            => r,
                g_in            => g,
                b_in            => b,
                r_out           => r2,
                g_out           => g2,
                b_out           => b2
            );

    lut_inst : lut
    port map(
                clk         => clk27mhz,
                rst         => rst,
                avg_r       => avg_r,
                avg_g       => avg_g,
                avg_b       => avg_b,
                dest_set    => dest_set,
                data8_r     => data8_r,
                data8_g     => data8_g,
                data8_b     => data8_b,
                dest_set_lut => dest_set_lut
            );

    seven_segment_controller_inst : seven_segment_controller
    port map(
                clk      => clk10khz,
                rst      => rst,
                input    => ss,
                ss_led1  => ss1,
                ss_led2  => ss2,
                ss_led3  => ss3,
                ss_led4  => ss4,
                ss_led5  => ss5,
                ss_led6  => ss6,
                ss_led7  => ss7,
                ss_led8  => ss8,
                ss_leda  => ssa,
                ss_ledb  => ssb,
                ss_ledc  => ssc,
                ss_ledd  => ssd,
                ss_lede  => sse,
                ss_ledf  => ssf,
                ss_ledg  => ssg,
                ss_leddp => ssdp
            );

    spi_inst : spi
    port map(
                clk            => clk27mhz,
                rst            => rst,
                sck            => sck,
                mosi           => mosi,
                dest_get       => dest_get,
                data8_r_get    => data8_r_get,
                data8_g_get    => data8_g_get,
                data8_b_get    => data8_b_get,
                start_transfer => vsync,
                dip_sw => not dip_sw
            );

    rgb_buffer_inst : rgb_buffer
    port map(
                clk         => clk27mhz,
                rst         => rst,
                data8_r_set => data8_r,
                data8_g_set => data8_g,
                data8_b_set => data8_b,
                dest_set    => dest_set_lut,
                data8_r_get => data8_r_get,
                data8_g_get => data8_g_get,
                data8_b_get => data8_b_get,
                dest_get    => dest_get
            );

    input_buf_datain(26 downto 20) <= r_raw;
    input_buf_datain(19)           <= '0';
    input_buf_datain(18 downto 12) <= g_raw;
    input_buf_datain(11)           <= '0';
    input_buf_datain(10 downto 4)  <= b_raw;
    input_buf_datain(3)            <= '0';
    input_buf_datain(2)            <= hsync_raw;
    input_buf_datain(1)            <= vsync_raw;
    input_buf_datain(0)            <= data_enable_raw;

    input_buf_inst : input_buf
    PORT MAP(
                datain                => input_buf_datain,
                dataout(26 downto 19) => r_buf,
                dataout(18 downto 11) => g_buf,
                dataout(10 downto 3)  => b_buf,
                dataout(2)            => hsync_buf,
                dataout(1)            => vsync_buf,
                dataout(0)            => data_enable_buf
            );

    input_ff_inst : input_buffer
    port map(
                clk             => clk27mhz,
                rst             => rst,
                r_buf           => r_buf,
                g_buf           => g_buf,
                b_buf           => b_buf,
                data_enable_buf => data_enable_buf,
                hsync_buf       => hsync_buf,
                vsync_buf       => vsync_buf,
                r               => r,
                g               => g,
                b               => b,
                hsync           => hsync,
                vsync           => vsync,
                data_enable     => data_enable,
                data_enable_syn => data_enable_syn
            );

    i2c_inst : i2c
    port map(
                rst     => rst,
                clk400k => clk400khz,
                scl     => scl,
                sda     => sda
            );

    i2c_io_buf_inst : i2c_io_buf
    port map(
                datain  => i2c_iobuf0_datain,
                dataout => i2c_iobuf0_dataout
            );

    spi_io_buf_inst : spi_io_buf
    port map(
                datain     => (0 => sck, 1 => mosi),
                dataout(0) => sck_buf,
                dataout(1) => mosi_buf
            );

    tvp7002_control_inst : tvp7002_control
    port map(
                rst      => rst,
                clk10khz => clk10khz,
                resetb   => resetb
            );

end behavioural;

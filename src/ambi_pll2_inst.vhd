ambi_pll2_inst : ambi_pll2 PORT MAP (
		areset	 => areset_sig,
		inclk0	 => inclk0_sig,
		c0	 => c0_sig,
		c1	 => c1_sig,
		c2	 => c2_sig
	);

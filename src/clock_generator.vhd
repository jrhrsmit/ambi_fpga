library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity clock_generator is 
	port(
		clk50mhz : in std_logic;
		rst : in std_logic;
		clk1hz : buffer std_logic;
		clk100hz : buffer std_logic;
		clk1khz : buffer std_logic;
		clk1mhz : buffer std_logic
	);
end entity clock_generator;

architecture behavioural of clock_generator is
	signal count1 : std_logic_vector(31 downto 0);
	signal count100 : std_logic_vector(31 downto 0);
	signal count1k : std_logic_vector(31 downto 0);
	signal count1m : std_logic_vector(31 downto 0);
	
begin
	process(clk50mhz, rst)
	begin
		if (rising_edge(clk50mhz)) then
			-- reset is active low
			if (rst = '0') then
				count1 <= (others => '0');
				clk1hz <= '0';
			else
				if(count1 = 25e6) then
					count1 <= (others => '0');
					clk1hz <= NOT(clk1hz);
				else
					count1 <= count1 + 1;
				end if;
			end if;
		end if;
	end process;
	process(clk50mhz, rst)
	begin
		if (rising_edge(clk50mhz)) then
			-- reset is active low
			if (rst = '0') then
				count1k <= (others => '0');
				clk1khz <= '0';
			else
				if(count1k = 25e3) then
					count1k <= (others => '0');
					clk1khz <= NOT(clk1khz);
				else
					count1k <= count1k + 1;
				end if;
			end if;
		end if;
	end process;
	process(clk50mhz, rst)
	begin
		if (rising_edge(clk50mhz)) then
			-- reset is active low
			if (rst = '0') then
				count100 <= (others => '0');
				clk100hz <= '0';
			else
				if(count100 = 250e3) then
					count100 <= (others => '0');
					clk100hz <= NOT(clk100hz);
				else
					count100 <= count100 + 1;
				end if;
			end if;
		end if;
	end process;
	process(clk50mhz, rst)
	begin
		if (rising_edge(clk50mhz)) then
			-- reset is active low
			if (rst = '0') then
				count1m <= (others => '0');
				clk1mhz <= '0';
			else
				if(count1m = 25) then
					count1m <= (others => '0');
					clk1mhz <= NOT(clk1mhz);
				else
					count1m <= count1m + 1;
				end if;
			end if;
		end if;
	end process;
end behavioural;
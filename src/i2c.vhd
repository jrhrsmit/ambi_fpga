library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_misc.all;

entity i2c is
	generic(
		I2C_DATA_IDX_MAX : integer := 64
	);
	port(
		rst     : in  std_logic;
		clk400k : in  std_logic;
		debug   : out std_logic;
		scl     : out std_logic;
		sda     : out std_logic
	);
end entity i2c;

architecture behavioural of i2c is

	type states is (countdown, idle, start_condition, repeated_start_condition, data, acknowledge, stop_condition);

	--	signal clk400k        : std_logic                    := '0';
	signal clkgen_counter : integer range 0 to 127              := 0;
	signal state          : states                              := countdown;
	signal countdown_tmr  : integer range 0 to 1600000          := 1600000; -- 800k = 2s
	signal numbytes       : integer range 0 to I2C_DATA_IDX_MAX := 0;
	signal numbits        : integer range 0 to 7                := 7;
	signal clk_quarter    : integer range 0 to 3                := 0;

	--	signal n_clk400k        : std_logic                    := '0';
	signal n_clkgen_counter : integer range 0 to 256              := 0;
	signal n_state          : states                              := countdown;
	signal n_countdown_tmr  : integer range 0 to 1600000          := 1600000; -- 800k = 2s
	signal n_numbytes       : integer range 0 to I2C_DATA_IDX_MAX := 0;
	signal n_numbits        : integer range 0 to 7                := 7;
	signal n_clk_quarter    : integer range 0 to 3                := 0;

	signal n_sda : std_logic;
	signal n_scl : std_logic;

	-- for 1080p60: 
	-- Frame rate	60
	-- Line rate 	67.5kHz
	-- Pixel rate	148.5MHz

	-- Recommended VCO Range and Charge Pump Current Settings (for 1080p60) (Table 4)
	-- PLLDIV 			01h [7:0]	89h
	-- PLLDIV			02h	[7:4] 	80h
	-- REG 				03h			E0h
	-- VCO RANGE		03h [7:6] 	11b
	-- CP CURRENT		03h [5:3]	100b
	-- OUTPUT DIVIDER 	04h	[0]		0h

	-- Typical embedded sync settings (Table 8)
	-- 15h	47h
	-- 40h	07h
	-- 41h 	01h
	-- 42h 	8Bh
	-- 43h	08h
	-- 44h	04h
	-- 45h 	04h
	-- 46h 	2Dh
	-- 47h	2Dh
	-- 48h 	00h
	-- 49h 	00h

	-- Sync seperator threshold
	-- 11h 	2Dh

	-- H-PLL Pre/Post-Coast settings
	-- 12h 	01h
	-- 13h 	00h

	-- Macrovision Stripper Width
	-- 34h	03h

	-- I2C data, including i2c device address and RW byte.
	-- Format:
	-- X COUNTDOWN ACK REPEATED_START <DATA x8>

	--/* Register parameters for 1080P60 */
	--static const struct i2c_reg_value tvp7002_parms_1080P60[] = {
	--	{ TVP7002_HPLL_FDBK_DIV_MSBS, 0x89, TVP7002_WRITE },
	--	{ TVP7002_HPLL_FDBK_DIV_LSBS, 0x80, TVP7002_WRITE },
	--	{ TVP7002_HPLL_CRTL, 0xE0, TVP7002_WRITE },
	--	{ TVP7002_AVID_START_PIXEL_LSBS, 0x06, TVP7002_WRITE },
	--	{ TVP7002_AVID_START_PIXEL_MSBS, 0x01, TVP7002_WRITE },
	--	{ TVP7002_AVID_STOP_PIXEL_LSBS, 0x8a, TVP7002_WRITE },
	--	{ TVP7002_AVID_STOP_PIXEL_MSBS, 0x08, TVP7002_WRITE },
	--	{ TVP7002_VBLK_F_0_START_L_OFF, 0x02, TVP7002_WRITE },
	--	{ TVP7002_VBLK_F_1_START_L_OFF, 0x02, TVP7002_WRITE },
	--	{ TVP7002_VBLK_F_0_DURATION, 0x16, TVP7002_WRITE },
	--	{ TVP7002_VBLK_F_1_DURATION, 0x17, TVP7002_WRITE },
	--	{ TVP7002_ALC_PLACEMENT, 0x5a, TVP7002_WRITE },
	--	{ TVP7002_CLAMP_START, 0x32, TVP7002_WRITE },
	--	{ TVP7002_CLAMP_W, 0x20, TVP7002_WRITE },
	--	{ TVP7002_HPLL_PRE_COAST, 0x01, TVP7002_WRITE },
	--	{ TVP7002_HPLL_POST_COAST, 0x00, TVP7002_WRITE },
	--	{ TVP7002_EOR, 0xff, TVP7002_RESERVED }
	--};

	type i2c_data_type is array (0 to I2C_DATA_IDX_MAX) of unsigned(11 downto 0);
	constant i2c_data : i2c_data_type := (
		x"0B8",                         -- addr + write
		x"001",                         -- PLLDIV addr
		x"089",                         -- 01
		x"080",                         -- 02
		x"0E0",                         -- 03
		x"000",                         -- 04
		x"002",                         -- 05 Clamp start at 0
		x"110",                         -- 06 Clamp width to half PC recommendations
		--		x"104",                         -- 07 HSOUT 4 pixels

		x"0B8",                         -- addr + write
		x"040",                         -- AVID start pix address
		--		x"007",                         -- 40 default fhd
		--		x"001",                         -- 41
		--		x"08B",                         -- 42
		--		x"008",                         -- 43
		--		x"004",                         -- 44
		--		x"004",                         -- 45
		--		x"02D",                         -- 46
		--		x"02D",                         -- 47
		-- total lines: 1125 - 0x04 - 0x2D = 1076
		-- total pixels: 0x187 - 0x2a = 349
		-- px per line should be 27/148.5*1920=349 px
		-- max value for all white: 5B4D750
		-- start = 2F
		x"02F",                         -- 40 settings for 27mhz clk LSB pixel back porch, AVID start px, confirmed with scope 
		x"000",                         -- 41 settings for 27mhz clk MSB pixel back porch, AVID start px, confirmed with scope
		x"089",                         -- 42 settings for 27mhz clk LSB pixel front porch, AVID stop px, confirmed with scope 
		x"001",                         -- 43 settings for 27mhz clk MSB pixel front porch, AVID stop px, confirmed with scope 
		x"004",                         -- 44 settings for 27mhz clk LSB line front porch, confirmed with scope 
		x"004",                         -- 45 settings for 27mhz clk MSB line front porch, confirmed with scope 
		x"02D",                         -- 46 settings for 27mhz clk LSB line back porch, confirmed with scope 
		x"02D",                         -- 47 settings for 27mhz clk MSB line back porch, confirmed with scope 
		x"000",                         -- 48
		x"100",                         -- 49, request repeated start afterwards

		x"0B8",                         -- addr + write
		x"01A",                         -- input mux sel 2
		x"1C8",                         -- 1A, CLK SET to EXT_CLK, (C8)
		-- set to CA to use H-PLL for adc samples

		x"0B8",                         -- addr + write
		x"00E",                         -- Write reg 0E Sync Control
		x"110",                         -- 0E, set HSYNC/VSYNC settings, 92 active low hsync, D2 active high hsync, 52 auto
		                                -- 10 maximum auto settings

		x"0B8",                         -- addr + write
		x"012",                         -- Write reg 12 H-PLL Pre-Coast
		x"101",                         -- 12, set pre-coast to 1

		x"0B8",                         -- addr + write
		x"021",                         -- Write reg 21 HSOUT start
		x"100",                         -- 21, set output delay of hsout to 0

		x"0B8",                         -- addr + write
		x"022",                         -- Write reg 21 Misc control 4
		x"104",                         -- 22, disable internal coast and macrovision

		-- disabled due to 27mhz adc clk
		--		x"0B8",                         -- addr + write
		--		x"02C",                         -- Write reg 2C ADC setup
		--		x"180",                         -- 2C, set adc bias for >110MSps	

		x"0B8",                         -- addr + write
		x"031",                         -- Write reg 31 ALC placement
		x"112",                         -- 31, set to PC recommendation

		x"0B8",                         -- addr + write
		x"018",                         -- Write reg 18 Misc control 3
		x"101",                         -- 18, clock adc data out on falling edge
		
		x"0B8",                         -- addr + write
		x"03F",                         -- Write reg 3F video bandwidth control
		x"10F",                         -- 3F, minimum bandwidth, max filtering

		-- fine clamp control
		x"0B8",                         -- addr + write
		x"02A",                         -- Write reg 2a FineClampControl
		x"107",                         -- 2a, enable fine clamps

		x"0B8",                         -- addr + write
		x"010",                         -- Write reg 10 Sync-On-GreenThreshold
		x"158",                         -- 10, set to 58, set fine clamps to bottom-level

		-- Offsets and gains
		x"0B8",                         -- addr + write
		x"01B",                         -- Write reg 1b blue/green coarse gain
		x"0BB",                         -- 1B, set to 1.7, for 580mVpp inputs
		x"10B",                         -- 1C, set to 1.7, for 580mVpp inputs
		--		
		--		x"0B8",                         -- addr + write
		--		x"01E",                         -- Write reg 1e Blue coarse offset
		--		x"000",                         -- 1e, blue coarse offset to 0
		--		x"030",                         -- 1f, green coarse offset to 0
		--		x"114",                         -- 20, red coarse offset to 0
		--
		x"0B8",                         -- addr + write
		x"008",                         -- Write reg 08 Blue fine gain
		x"0FF",                         -- 08, blue fine gain
		x"0FF",                         -- 09, green fine gain
		x"1FF",                         -- 0a, red fine gain

		-- Output settings
		x"0B8",                         -- addr + write
		x"015",                         -- output fmt reg
		x"104",                         -- 15, set fine clamp to leading edge hsync reference

		x"0B8",                         -- addr + write
		x"017",                         -- Write reg 17 Misc control 2
		x"012"                          -- 17, enable outputs
		--				x"062"                          -- 17, enable outputs

		--		-- from the linux kernel (26 bytes + 3 bytes for output enable):
		--		x"0B8",
		--		x"001",
		--		x"089",                         --	{ TVP7002_HPLL_FDBK_DIV_MSBS, 0x89
		--		x"080",                         --	{ TVP7002_HPLL_FDBK_DIV_LSBS, 0x80
		--		x"1E0",                         --	{ TVP7002_HPLL_CRTL, 0xE0
		--		x"0B8",
		--		x"040",
		--		x"006",                         --	{ TVP7002_AVID_START_PIXEL_LSBS, 0x06
		--		x"001",                         --	{ TVP7002_AVID_START_PIXEL_MSBS, 0x01
		--		x"08A",                         --	{ TVP7002_AVID_STOP_PIXEL_LSBS, 0x8a
		--		x"008",                         --	{ TVP7002_AVID_STOP_PIXEL_MSBS, 0x08
		--		x"002",                         --	{ TVP7002_VBLK_F_0_START_L_OFF, 0x02
		--		x"002",                         --	{ TVP7002_VBLK_F_1_START_L_OFF, 0x02
		--		x"016",                         --	{ TVP7002_VBLK_F_0_DURATION, 0x16
		--		x"117",                         --	{ TVP7002_VBLK_F_1_DURATION, 0x17
		--		x"0B8",
		--		x"031",
		--		x"15A",                         --	{ TVP7002_ALC_PLACEMENT, 0x5a
		--		x"0B8",
		--		x"005",
		--		x"032",                         --	{ TVP7002_CLAMP_START, 0x32
		--		x"120",                         --	{ TVP7002_CLAMP_W, 0x20
		--		x"0B8",
		--		x"012",
		--		x"001",                         --	{ TVP7002_HPLL_PRE_COAST, 0x01
		--		x"100",                         --	{ TVP7002_HPLL_POST_COAST, 0x00		
		--		x"0B8",                         -- addr + write
		--		x"017",                         -- Write reg 17 Misc control 2
		--		x"402",                         -- 17, enable outputs

		-- reading out everything (128 bytes):
		--		x"0B8",                         -- read out literally everything
		--		x"100",
		--		x"0B9",                         -- addr + read
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"1FF",
		--		x"0B8",                         -- read out literally everything, 94 bytes, 91 registers
		--		x"108",
		--		x"0B9",                         -- addr + read
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"1FF",
		--		x"0B8",                         -- read out literally everything, 94 bytes, 91 registers
		--		x"110",
		--		x"0B9",                         -- addr + read
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"1FF",
		--		x"0B8",                         -- read out literally everything, 94 bytes, 91 registers
		--		x"118",
		--		x"0B9",                         -- addr + read
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"1FF",
		--		x"0B8",                         -- read out literally everything, 94 bytes, 91 registers
		--		x"120",
		--		x"0B9",                         -- addr + read
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"1FF",
		--		x"0B8",                         -- read out literally everything, 94 bytes, 91 registers
		--		x"128",
		--		x"0B9",                         -- addr + read
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"1FF",
		--		x"0B8",                         -- read out literally everything, 94 bytes, 91 registers
		--		x"130",
		--		x"0B9",                         -- addr + read
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"1FF",
		--		x"0B8",                         -- read out literally everything, 94 bytes, 91 registers
		--		x"138",
		--		x"0B9",                         -- addr + read
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"1FF",
		--		x"0B8",                         -- read out literally everything, 94 bytes, 91 registers
		--		x"140",
		--		x"0B9",                         -- addr + read
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"1FF",
		--		x"0B8",                         -- read out literally everything, 94 bytes, 91 registers
		--		x"148",
		--		x"0B9",                         -- addr + read
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"1FF",
		--		x"0B8",                         -- read out literally everything, 94 bytes, 91 registers
		--		x"150",
		--		x"0B9",                         -- addr + read
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"1FF",
		--		x"0B8",                         -- read out literally everything, 94 bytes, 91 registers
		--		x"158",
		--		x"0B9",                         -- addr + read
		--		x"2FF",
		--		x"2FF",
		--		x"2FF",
		--		x"0FF"
	);

begin

	-- reset process, set SDA and SCL Hi-Z, i.e. open-drain high on reset
	process(rst, clk400k, n_scl, n_sda, n_numbytes, n_state, n_countdown_tmr, n_clk_quarter)
	begin
		if (rising_edge(clk400k)) then
			-- reset is active low
			if (rst = '0') then
				scl           <= '1';
				sda           <= '1';
				numbytes      <= 0;
				numbits       <= 7;
				state         <= countdown;
				countdown_tmr <= 1600000;
				clk_quarter   <= 0;
			else
				scl           <= n_scl;
				sda           <= n_sda;
				numbytes      <= n_numbytes;
				numbits       <= n_numbits;
				state         <= n_state;
				countdown_tmr <= n_countdown_tmr;
				clk_quarter   <= n_clk_quarter;
			end if;
		end if;
	end process;

	process(state)
	begin
		if (state = countdown) then
			debug <= '1';
		else
			debug <= '0';
		end if;
	end process;

	process(rst, clk400k, state, countdown_tmr, clk_quarter, numbytes, numbits)
	begin
		case state is
			when countdown =>
				n_sda         <= '1';
				n_scl         <= '1';
				n_numbytes    <= numbytes;
				n_numbits     <= 7;
				n_clk_quarter <= 0;
				if (countdown_tmr = 0) then
					n_countdown_tmr <= 0;
					n_state         <= start_condition;
				else
					n_countdown_tmr <= countdown_tmr - 1;
					n_state         <= countdown;
				end if;
			when start_condition =>
				-- generate a start condition by pulling SDA low first, then
				-- SCL, and after all that go to the data state
				n_numbytes      <= numbytes;
				n_numbits       <= 7;
				n_countdown_tmr <= 1600000;
				case clk_quarter is
					when 0 =>
						n_scl         <= '1';
						n_sda         <= '0';
						n_clk_quarter <= clk_quarter + 1;
						n_state       <= start_condition;
					when 1 =>
						n_scl         <= '1';
						n_sda         <= '0';
						n_clk_quarter <= clk_quarter + 1;
						n_state       <= start_condition;
					when 2 =>
						n_scl         <= '1';
						n_sda         <= '0';
						n_clk_quarter <= clk_quarter + 1;
						n_state       <= start_condition;
					when 3 =>
						n_scl         <= '0';
						n_sda         <= '0';
						n_clk_quarter <= 0;
						n_state       <= data;
				end case;
			when repeated_start_condition =>
				-- generate a start condition by pulling SDA down in
				-- the middle of SCL high
				n_numbytes      <= numbytes;
				n_numbits       <= 7;
				n_countdown_tmr <= 1600000;
				case clk_quarter is
					when 0 =>
						n_scl         <= '0';
						n_sda         <= '1';
						n_clk_quarter <= clk_quarter + 1;
						n_state       <= repeated_start_condition;
					when 1 =>
						n_scl         <= '1';
						n_sda         <= '1';
						n_clk_quarter <= clk_quarter + 1;
						n_state       <= repeated_start_condition;
					when 2 =>
						n_scl         <= '1';
						n_sda         <= '0';
						n_clk_quarter <= clk_quarter + 1;
						n_state       <= repeated_start_condition;
					when 3 =>
						n_scl         <= '0';
						n_sda         <= '0';
						n_clk_quarter <= 0;
						n_state       <= data;
				end case;
			when data =>
				-- set SDA to current data, then go to SCL pulse, lastly
				-- determine the next state
				n_numbytes      <= numbytes;
				n_countdown_tmr <= 1600000;
				case clk_quarter is
					when 0 =>
						n_numbits     <= numbits;
						n_scl         <= '0';
						n_sda         <= i2c_data(numbytes)(numbits);
						n_state       <= data;
						n_clk_quarter <= clk_quarter + 1;
					when 1 =>
						n_numbits     <= numbits;
						n_scl         <= '1';
						n_sda         <= i2c_data(numbytes)(numbits);
						n_state       <= data;
						n_clk_quarter <= clk_quarter + 1;
					when 2 =>
						n_numbits     <= numbits;
						n_scl         <= '1';
						n_sda         <= i2c_data(numbytes)(numbits);
						n_state       <= data;
						n_clk_quarter <= clk_quarter + 1;
					when 3 =>
						n_scl         <= '0';
						n_sda         <= i2c_data(numbytes)(numbits);
						n_clk_quarter <= 0;
						if (numbits = 0) then
							n_numbits <= 7;
							n_state   <= acknowledge;
						else
							n_numbits <= numbits - 1;
							n_state   <= data;
						end if;

				end case;
			when acknowledge =>
				-- just keep Hi-Z (slave controls SDA here) and determine if
				-- we have to got to STOP or for another data round
				n_numbits       <= 7;
				n_countdown_tmr <= 1600000;
				-- bit 9 represents if we need to ACK or not
				if (i2c_data(numbytes)(9) = '1') then
					n_sda <= '0';
				else
					n_sda <= '1';
				end if;
				case clk_quarter is
					when 0 =>
						n_numbytes      <= numbytes;
						n_scl           <= '0';
						n_state         <= acknowledge;
						n_clk_quarter   <= clk_quarter + 1;
						n_countdown_tmr <= 0;
					when 1 =>
						n_numbytes      <= numbytes;
						n_scl           <= '1';
						n_state         <= acknowledge;
						n_clk_quarter   <= clk_quarter + 1;
						n_countdown_tmr <= 0;
					when 2 =>
						n_numbytes      <= numbytes;
						n_scl           <= '1';
						n_state         <= acknowledge;
						n_clk_quarter   <= clk_quarter + 1;
						n_countdown_tmr <= 0;
					when 3 =>
						n_scl         <= '0';
						n_clk_quarter <= 0;
						if (numbytes = I2C_DATA_IDX_MAX) then
							n_numbytes <= numbytes;
							n_state    <= stop_condition;
						else
							n_numbytes <= numbytes + 1;
							-- bit 8 represents that the next byte needs a repeated start first
							-- bit 10 is stop condition -> countdown -> start condition
							if (i2c_data(numbytes)(10) = '1') then
								n_state <= stop_condition;
							elsif (i2c_data(numbytes)(8) = '1') then
								n_state <= repeated_start_condition;
							else
								n_state <= data;
							end if;
						end if;
				end case;
			when stop_condition =>
				n_numbytes      <= numbytes;
				n_numbits       <= 7;
				n_countdown_tmr <= countdown_tmr;
				case clk_quarter is
					when 0 =>
						n_sda         <= '0';
						n_scl         <= '0';
						n_state       <= stop_condition;
						n_clk_quarter <= clk_quarter + 1;
					when 1 =>
						n_sda         <= '0';
						n_scl         <= '1';
						n_state       <= stop_condition;
						n_clk_quarter <= clk_quarter + 1;
					when 2 =>
						n_sda         <= '1';
						n_scl         <= '1';
						n_state       <= stop_condition;
						n_clk_quarter <= clk_quarter + 1;
					when 3 =>
						n_sda         <= '1';
						n_scl         <= '1';
						n_state       <= idle;
						n_clk_quarter <= 0;
						if (numbytes = I2C_DATA_IDX_MAX) then
							n_state <= idle;
						elsif (i2c_data(numbytes - 1)(10) = '1') then
							n_state <= countdown;
						else
							n_state <= start_condition;
						end if;
				end case;
			when idle =>
				n_numbytes      <= 0;
				n_numbits       <= 7;
				n_countdown_tmr <= 0;
				n_sda           <= '1';
				n_scl           <= '1';
				n_state         <= idle;
				n_clk_quarter   <= 0;
		end case;
	end process;

end behavioural;

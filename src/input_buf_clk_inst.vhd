input_buf_clk_inst : input_buf_clk PORT MAP (
		datain	 => datain_sig,
		inclock	 => inclock_sig,
		dataout_h	 => dataout_h_sig,
		dataout_l	 => dataout_l_sig
	);

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity input_buffer is
	port(
		clk             : in  std_logic;
		rst             : in  std_logic;
		r_buf           : in  std_logic_vector(7 downto 0);
		g_buf           : in  std_logic_vector(7 downto 0);
		b_buf           : in  std_logic_vector(7 downto 0);
		data_enable_buf : in  std_logic;
		hsync_buf       : in  std_logic;
		vsync_buf       : in  std_logic;
		r               : out unsigned(7 downto 0);
		g               : out unsigned(7 downto 0);
		b               : out unsigned(7 downto 0);
		hsync           : out std_logic;
		vsync           : out std_logic;
		data_enable     : out std_logic;
		data_enable_syn : out std_logic
	);
end entity input_buffer;

architecture behavioural of input_buffer is
	signal n_data_enable_syn : std_logic := '0';

	type states is (de_wait_for_rising, de_wait_for_falling);
	signal state               : states    := de_wait_for_rising;
	signal n_state             : states    := de_wait_for_rising;
	signal data_enable_syn_buf : std_logic := '0';

	signal counter   : integer range 0 to 128 := 0;
	signal n_counter : integer range 0 to 128 := 0;
begin
	data_enable_syn <= data_enable_syn_buf;
	process(clk, rst)
	begin
		if (rising_edge(clk)) then
			-- reset is active low
			if (rst = '0') then
				state               <= de_wait_for_rising;
				r                   <= (others => '0');
				g                   <= (others => '0');
				b                   <= (others => '0');
				hsync               <= '0';
				vsync               <= '0';
				data_enable         <= '1';
				data_enable_syn_buf <= '1';
				counter             <= 0;
			else
				state               <= n_state;
				r                   <= unsigned(r_buf);
				g                   <= unsigned(g_buf);
				b                   <= unsigned(b_buf);
				hsync               <= hsync_buf;
				vsync               <= vsync_buf;
				data_enable         <= data_enable_buf;
				data_enable_syn_buf <= n_data_enable_syn or data_enable_buf;
				counter             <= n_counter;

			end if;
		end if;
	end process;

	-- synthesize data_enable by orring all inputs
	process(clk, r_buf, g_buf, b_buf, data_enable_syn_buf, state, data_enable_buf, counter)
		variable temp : std_logic := '0';
	begin
		case state is
			when de_wait_for_rising =>
				if (counter > 64) then
					n_counter         <= 0;
					n_state           <= de_wait_for_falling;
					n_data_enable_syn <= '1';
				else
					n_state           <= de_wait_for_rising;
					n_data_enable_syn <= '0';
					if (data_enable_buf = '1') then
						n_counter <= counter + 1;
					else
						n_counter <= 0;
					end if;
				end if;
				temp := '0';
			--				for i in 5 to 7 loop
			--					temp := temp or r_buf(i) or g_buf(i) or b_buf(i);
			--				end loop;
			when de_wait_for_falling =>
				temp := '0';
				for i in 4 to 7 loop
					temp := temp or r_buf(i) or g_buf(i) or b_buf(i);
				end loop;
				if (counter = 8) then
					n_counter         <= 0;
					n_state           <= de_wait_for_rising;
					n_data_enable_syn <= '0';
				elsif (data_enable_buf = '1') then
					n_counter         <= 0;
					n_state           <= de_wait_for_falling;
					n_data_enable_syn <= '0';
				elsif (temp = '1') then
					n_counter         <= counter + 1;
					n_state           <= de_wait_for_falling;
					n_data_enable_syn <= '1';
				else
					n_counter         <= 0;
					n_state           <= de_wait_for_rising;
					n_data_enable_syn <= '0';
				end if;

		end case;
	end process;

end behavioural;

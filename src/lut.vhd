library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_misc.all;

entity lut is
	port(
		clk         : in  std_logic;
		rst         : in  std_logic;
		avg_r       : in  unsigned(7 downto 0);
		avg_g       : in  unsigned(7 downto 0);
		avg_b       : in  unsigned(7 downto 0);
		dest_set    : in  unsigned(7 downto 0);
		data8_r     : out unsigned(7 downto 0);
		data8_g     : out unsigned(7 downto 0);
		data8_b     : out unsigned(7 downto 0);
		dest_set_lut : out unsigned(7 downto 0)
	);
end entity lut;

architecture behavioural of lut is
    type lut_t is array (0 to 255) of unsigned (7 downto 0);
    signal lutr : lut_t := (
        x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
        x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"01",
        x"01", x"01", x"01", x"01", x"01", x"01", x"01", x"01",
        x"01", x"02", x"02", x"02", x"02", x"02", x"02", x"02",
        x"03", x"03", x"03", x"03", x"03", x"04", x"04", x"04",
        x"04", x"05", x"05", x"05", x"05", x"06", x"06", x"06",
        x"06", x"07", x"07", x"07", x"08", x"08", x"08", x"09",
        x"09", x"09", x"0A", x"0A", x"0B", x"0B", x"0B", x"0C",
        x"0C", x"0D", x"0D", x"0D", x"0E", x"0E", x"0F", x"0F",
        x"10", x"10", x"11", x"11", x"12", x"12", x"13", x"13",
        x"14", x"14", x"15", x"16", x"16", x"17", x"17", x"18",
        x"19", x"19", x"1A", x"1A", x"1B", x"1C", x"1C", x"1D",
        x"1E", x"1E", x"1F", x"20", x"21", x"21", x"22", x"23",
        x"23", x"24", x"25", x"26", x"27", x"27", x"28", x"29",
        x"2A", x"2B", x"2B", x"2C", x"2D", x"2E", x"2F", x"30",
        x"31", x"31", x"32", x"33", x"34", x"35", x"36", x"37",
        x"38", x"39", x"3A", x"3B", x"3C", x"3D", x"3E", x"3F",
        x"40", x"41", x"42", x"43", x"44", x"45", x"46", x"47",
        x"49", x"4A", x"4B", x"4C", x"4D", x"4E", x"4F", x"51",
        x"52", x"53", x"54", x"55", x"57", x"58", x"59", x"5A",
        x"5B", x"5D", x"5E", x"5F", x"61", x"62", x"63", x"64",
        x"66", x"67", x"69", x"6A", x"6B", x"6D", x"6E", x"6F",
        x"71", x"72", x"74", x"75", x"77", x"78", x"79", x"7B",
        x"7C", x"7E", x"7F", x"81", x"82", x"84", x"85", x"87",
        x"89", x"8A", x"8C", x"8D", x"8F", x"91", x"92", x"94",
        x"95", x"97", x"99", x"9A", x"9C", x"9E", x"9F", x"A1",
        x"A3", x"A5", x"A6", x"A8", x"AA", x"AC", x"AD", x"AF",
        x"B1", x"B3", x"B5", x"B6", x"B8", x"BA", x"BC", x"BE",
        x"C0", x"C2", x"C4", x"C5", x"C7", x"C9", x"CB", x"CD",
        x"CF", x"D1", x"D3", x"D5", x"D7", x"D9", x"DB", x"DD",
        x"DF", x"E1", x"E3", x"E5", x"E7", x"EA", x"EC", x"EE",
        x"F0", x"F2", x"F4", x"F6", x"F8", x"FB", x"FD", x"FF");

    signal lutg : lut_t := (
        x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
        x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"01",
        x"01", x"01", x"01", x"01", x"01", x"01", x"01", x"01",
        x"01", x"02", x"02", x"02", x"02", x"02", x"02", x"02",
        x"03", x"03", x"03", x"03", x"03", x"04", x"04", x"04",
        x"04", x"05", x"05", x"05", x"05", x"06", x"06", x"06",
        x"06", x"07", x"07", x"07", x"08", x"08", x"08", x"09",
        x"09", x"09", x"0A", x"0A", x"0B", x"0B", x"0B", x"0C",
        x"0C", x"0D", x"0D", x"0D", x"0E", x"0E", x"0F", x"0F",
        x"10", x"10", x"11", x"11", x"12", x"12", x"13", x"13",
        x"14", x"14", x"15", x"16", x"16", x"17", x"17", x"18",
        x"19", x"19", x"1A", x"1A", x"1B", x"1C", x"1C", x"1D",
        x"1E", x"1E", x"1F", x"20", x"21", x"21", x"22", x"23",
        x"23", x"24", x"25", x"26", x"27", x"27", x"28", x"29",
        x"2A", x"2B", x"2B", x"2C", x"2D", x"2E", x"2F", x"30",
        x"31", x"31", x"32", x"33", x"34", x"35", x"36", x"37",
        x"38", x"39", x"3A", x"3B", x"3C", x"3D", x"3E", x"3F",
        x"40", x"41", x"42", x"43", x"44", x"45", x"46", x"47",
        x"49", x"4A", x"4B", x"4C", x"4D", x"4E", x"4F", x"51",
        x"52", x"53", x"54", x"55", x"57", x"58", x"59", x"5A",
        x"5B", x"5D", x"5E", x"5F", x"61", x"62", x"63", x"64",
        x"66", x"67", x"69", x"6A", x"6B", x"6D", x"6E", x"6F",
        x"71", x"72", x"74", x"75", x"77", x"78", x"79", x"7B",
        x"7C", x"7E", x"7F", x"81", x"82", x"84", x"85", x"87",
        x"89", x"8A", x"8C", x"8D", x"8F", x"91", x"92", x"94",
        x"95", x"97", x"99", x"9A", x"9C", x"9E", x"9F", x"A1",
        x"A3", x"A5", x"A6", x"A8", x"AA", x"AC", x"AD", x"AF",
        x"B1", x"B3", x"B5", x"B6", x"B8", x"BA", x"BC", x"BE",
        x"C0", x"C2", x"C4", x"C5", x"C7", x"C9", x"CB", x"CD",
        x"CF", x"D1", x"D3", x"D5", x"D7", x"D9", x"DB", x"DD",
        x"DF", x"E1", x"E3", x"E5", x"E7", x"EA", x"EC", x"EE",
        x"F0", x"F2", x"F4", x"F6", x"F8", x"FB", x"FD", x"FF");

    signal lutb : lut_t := (
        x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"00",
        x"00", x"00", x"00", x"00", x"00", x"00", x"00", x"01",
        x"01", x"01", x"01", x"01", x"01", x"01", x"01", x"01",
        x"01", x"02", x"02", x"02", x"02", x"02", x"02", x"02",
        x"03", x"03", x"03", x"03", x"03", x"04", x"04", x"04",
        x"04", x"05", x"05", x"05", x"05", x"06", x"06", x"06",
        x"06", x"07", x"07", x"07", x"08", x"08", x"08", x"09",
        x"09", x"09", x"0A", x"0A", x"0B", x"0B", x"0B", x"0C",
        x"0C", x"0D", x"0D", x"0D", x"0E", x"0E", x"0F", x"0F",
        x"10", x"10", x"11", x"11", x"12", x"12", x"13", x"13",
        x"14", x"14", x"15", x"16", x"16", x"17", x"17", x"18",
        x"19", x"19", x"1A", x"1A", x"1B", x"1C", x"1C", x"1D",
        x"1E", x"1E", x"1F", x"20", x"21", x"21", x"22", x"23",
        x"23", x"24", x"25", x"26", x"27", x"27", x"28", x"29",
        x"2A", x"2B", x"2B", x"2C", x"2D", x"2E", x"2F", x"30",
        x"31", x"31", x"32", x"33", x"34", x"35", x"36", x"37",
        x"38", x"39", x"3A", x"3B", x"3C", x"3D", x"3E", x"3F",
        x"40", x"41", x"42", x"43", x"44", x"45", x"46", x"47",
        x"49", x"4A", x"4B", x"4C", x"4D", x"4E", x"4F", x"51",
        x"52", x"53", x"54", x"55", x"57", x"58", x"59", x"5A",
        x"5B", x"5D", x"5E", x"5F", x"61", x"62", x"63", x"64",
        x"66", x"67", x"69", x"6A", x"6B", x"6D", x"6E", x"6F",
        x"71", x"72", x"74", x"75", x"77", x"78", x"79", x"7B",
        x"7C", x"7E", x"7F", x"81", x"82", x"84", x"85", x"87",
        x"89", x"8A", x"8C", x"8D", x"8F", x"91", x"92", x"94",
        x"95", x"97", x"99", x"9A", x"9C", x"9E", x"9F", x"A1",
        x"A3", x"A5", x"A6", x"A8", x"AA", x"AC", x"AD", x"AF",
        x"B1", x"B3", x"B5", x"B6", x"B8", x"BA", x"BC", x"BE",
        x"C0", x"C2", x"C4", x"C5", x"C7", x"C9", x"CB", x"CD",
        x"CF", x"D1", x"D3", x"D5", x"D7", x"D9", x"DB", x"DD",
        x"DF", x"E1", x"E3", x"E5", x"E7", x"EA", x"EC", x"EE",
        x"F0", x"F2", x"F4", x"F6", x"F8", x"FB", x"FD", x"FF");

    attribute ramstyle : string;
    attribute ramstyle of lutr : signal is "M9K";
    attribute ramstyle of lutg : signal is "M9K";
    attribute ramstyle of lutb : signal is "M9K";
    signal data8_r_n : unsigned(7 downto 0) := x"00";
    signal data8_g_n : unsigned(7 downto 0) := x"00";
    signal data8_b_n : unsigned(7 downto 0) := x"00";
    signal dest_set_lut_n : unsigned(7 downto 0) := x"00";

begin

    -- reset process
	process(clk, rst)
	begin
		if (rising_edge(clk)) then
			if (rst = '0') then
                dest_set_lut <= x"00";
                data8_r <= x"00";
                data8_g <= x"00";
                data8_b <= x"00";
			else
                dest_set_lut <= dest_set_lut_n;
                data8_r <= data8_r_n;
                data8_g <= data8_g_n;
                data8_b <= data8_b_n;
			end if;
		end if;
	end process;

    -- state machine
    process(avg_r, avg_g, avg_b, dest_set)
    begin
        data8_r_n <= lutr(to_integer(avg_r));
        data8_g_n <= lutg(to_integer(avg_g));
        data8_b_n <= lutb(to_integer(avg_b));
        dest_set_lut_n <= dest_set;
    end process;

end behavioural;

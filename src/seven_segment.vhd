library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity seven_segment is 
	port(
		clk : in std_logic;
		rst : in std_logic;
		number : in std_logic_vector (3 downto 0);
		ssegment : out std_logic_vector(6 downto 0)
	);
end entity seven_segment;

architecture behavioural of seven_segment is
	
begin
	process(clk, rst)
	begin
		if (rising_edge(clk)) then
			-- reset is active low
			if (rst = '0') then
				ssegment <= (others => '1');
			else
				case number is
					when "0000" =>
						ssegment <= "1000000";
					when "0001" =>
						ssegment <= "1111001";
					when "0010" =>
						ssegment <= "0100100";
					when "0011" => 
						ssegment <= "0110000";
					when "0100" => 
						ssegment <= "0011001";
					when "0101" => 
						ssegment <= "0010010";
					when "0110" => 
						ssegment <= "0000010";
					when "0111" => 
						ssegment <= "1111000";
					when "1000" => 
						ssegment <= "0000000";
					when "1001" => 
						ssegment <= "0010000";
					when "1010" => 
						ssegment <= "0001000";
					when "1011" => 
						ssegment <= "0000011";
					when "1100" => 
						ssegment <= "1000110";
					when "1101" => 
						ssegment <= "0100001";
					when "1110" => 
						ssegment <= "0000110";
					when "1111" => 
						ssegment <= "0001110";
					when others =>
						ssegment <= "1111111";
				end case;
			end if;
		end if;
	end process;
end behavioural;

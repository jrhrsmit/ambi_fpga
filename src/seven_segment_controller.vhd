library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_misc.all;
--use IEEE.std_logic_arith.all; 

entity seven_segment_controller is 
	port(
		clk : in std_logic;
		rst : in std_logic;
		input : in std_logic_vector (31 downto 0);
        ss_led1 : out std_logic;
        ss_led2 : out std_logic;
        ss_led3 : out std_logic;
        ss_led4 : out std_logic;
        ss_led5 : out std_logic;
        ss_led6 : out std_logic;
        ss_led7 : out std_logic;
        ss_led8 : out std_logic;
        ss_leda : out std_logic;
        ss_ledb : out std_logic;
        ss_ledc : out std_logic;
        ss_ledd : out std_logic;
        ss_lede : out std_logic;
        ss_ledf : out std_logic;
        ss_ledg : out std_logic;
        ss_leddp : out std_logic
	);
end entity seven_segment_controller;

architecture behavioural of seven_segment_controller is
	
    component seven_segment is
		port(
			clk : in std_logic;
			rst : in std_logic;
			number : in std_logic_vector (3 downto 0);
			ssegment : out std_logic_vector(6 downto 0)
		);
	end component seven_segment;

	signal count : integer range 0 to 8 := 0;
	signal n_count : integer range 0 to 8;
	signal input4 : std_logic_vector(3 downto 0);
	signal ssegment : std_logic_vector(6 downto 0);
	signal ssdigit : unsigned(7 downto 0);
	signal n_ssdigit : unsigned(7 downto 0);
	signal ss_digit_visible : std_logic_vector(7 downto 0);
	
	
begin
	ss_leddp <= '1';
	ss_leda <= ssegment(0);
	ss_ledb <= ssegment(1);
	ss_ledc <= ssegment(2);
	ss_ledd <= ssegment(3);
	ss_lede <= ssegment(4);
	ss_ledf <= ssegment(5);
	ss_ledg <= ssegment(6);
	
	ss_led1 <= ssdigit(0);
	ss_led2 <= ssdigit(1) or not ss_digit_visible(1);
	ss_led3 <= ssdigit(2) or not ss_digit_visible(2);
	ss_led4 <= ssdigit(3) or not ss_digit_visible(3);
	ss_led5 <= ssdigit(4) or not ss_digit_visible(4);
	ss_led6 <= ssdigit(5) or not ss_digit_visible(5);
	ss_led7 <= ssdigit(6) or not ss_digit_visible(6);
	ss_led8 <= ssdigit(7) or not ss_digit_visible(7);
	
	ss_digit_visible(7) <= or_reduce(input(31 downto 28));
	ss_digit_visible(6) <= ss_digit_visible(7) or or_reduce(input(27 downto 24));
	ss_digit_visible(5) <= ss_digit_visible(6) or or_reduce(input(23 downto 20));
	ss_digit_visible(4) <= ss_digit_visible(5) or or_reduce(input(19 downto 16));
	ss_digit_visible(3) <= ss_digit_visible(4) or or_reduce(input(15 downto 12));
	ss_digit_visible(2) <= ss_digit_visible(3) or or_reduce(input(11 downto 8));
	ss_digit_visible(1) <= ss_digit_visible(2) or or_reduce(input(7 downto 4));
	
	seven_segment0 : seven_segment port map (
        clk => clk,
        rst => rst,
        number => input4,
        ssegment => ssegment
	);

	process(clk, rst)
	begin
		if (rising_edge(clk)) then
			-- reset is active low
			if (rst = '0') then
				--ssegment <= (others => '1');
				count <= 0;
				ssdigit <= (others => '1');
			else
            count <= n_count;
				ssdigit <= n_ssdigit;
			end if;
		end if;
	end process;
	
	process(count, input, ssdigit)
	begin
		case count is
			-- 0 is off state
			when 0 =>
					n_ssdigit <= "01111111";
					n_count <= 1;
					input4 <= input(3 downto 0);
			when 1 =>
				n_ssdigit <= ssdigit rol 1;
				n_count <= 2;
				input4 <= input(3 downto 0);
			when 2 =>
				n_ssdigit <= ssdigit rol 1;
				n_count <= 3;
				input4 <= input(7 downto 4);
			when 3 =>
				n_ssdigit <= ssdigit rol 1;
				n_count <= 4;
				input4 <= input(11 downto 8);
			when 4 =>
				n_ssdigit <= ssdigit rol 1;
				n_count <= 5;
				input4 <= input(15 downto 12);
			when 5 =>
				n_ssdigit <= ssdigit rol 1;
				n_count <= 6;
				input4 <= input(19 downto 16);
			when 6 =>
				n_ssdigit <= ssdigit rol 1;
				n_count <= 7;
				input4 <= input(23 downto 20);
			when 7 =>
				n_ssdigit <= ssdigit rol 1;
				n_count <= 8;
				input4 <= input(27 downto 24);
			when 8 =>
				n_ssdigit <= ssdigit rol 1;
				n_count <= 1;
				input4 <= input(31 downto 28);
		end case;
	end process;
end behavioural;

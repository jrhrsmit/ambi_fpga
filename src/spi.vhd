library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_misc.all;

entity spi is
	port(
		clk            : in  std_logic;
		rst            : in  std_logic;
		sck            : out std_logic;
		mosi           : out std_logic;
		data8_r_get    : in  unsigned(7 downto 0);
		data8_g_get    : in  unsigned(7 downto 0);
		data8_b_get    : in  unsigned(7 downto 0);
		dest_get       : out unsigned(7 downto 0);
		start_transfer : in  std_logic;
        dip_sw : in std_logic_vector(7 downto 0)
	);
end entity spi;

architecture behavioural of spi is
	-- plus 1 for start frame
	constant num_leds : integer              := (17 * 2 + 31 * 2);
	signal byte0    : unsigned(7 downto 0) := x"E0";

	type states is (idle, wait_for_start_transfer, start_frame, transfer, end_frame);
	signal state      : states               := idle;
	signal n_state    : states               := idle;
	signal n_dest_get : unsigned(7 downto 0) := x"FF";
	signal n_mosi     : std_logic            := '1';
	signal n_sck      : std_logic            := '1';

	signal ctr_led       : unsigned(7 downto 0)   := x"00";
	signal n_ctr_led     : unsigned(7 downto 0)   := x"00";
	signal ctr_bit       : integer range 0 to 127 := 0;
	signal n_ctr_bit     : integer range 0 to 127 := 0;
	signal clk_quarter   : integer range 0 to 3   := 0;
	signal n_clk_quarter : integer range 0 to 3   := 0;
	signal ctr_byte      : integer range 0 to 3   := 0;
	signal n_ctr_byte    : integer range 0 to 3   := 0;

begin
	process(clk, rst)
	begin
		if (rising_edge(clk)) then
			if (rst = '0') then
				ctr_led     <= x"00";
				ctr_byte    <= 0;
				ctr_bit     <= 0;
				clk_quarter <= 0;
				mosi        <= '1';
				sck         <= '1';
				dest_get    <= x"FF";
				state       <= idle;
			else
				ctr_led     <= n_ctr_led;
				ctr_byte    <= n_ctr_byte;
				ctr_bit     <= n_ctr_bit;
				clk_quarter <= n_clk_quarter;
				mosi        <= n_mosi;
				sck         <= n_sck;
				dest_get    <= n_dest_get;
				state       <= n_state;
			end if;
		end if;
	end process;

    byte0(7 downto 5) <= "111";
    -- brightness
    byte0(4 downto 0) <= (others => '1');

	-- output control
	process(state, ctr_led, ctr_byte, ctr_bit, clk_quarter, start_transfer, data8_r_get, data8_g_get, data8_b_get)
		variable tmp : std_logic_vector(7 downto 0) := x"00";
	begin
		case state is
			when idle =>
				n_ctr_led     <= x"00";
				n_dest_get    <= x"FF";
				n_ctr_byte    <= 0;
				n_ctr_led     <= x"00";
				n_clk_quarter <= 0;
				n_ctr_bit     <= 0;
				n_sck         <= '1';
				n_mosi        <= '1';
				if (start_transfer = '0') then
					n_state <= wait_for_start_transfer;
				else
					n_state <= idle;
				end if;

			when wait_for_start_transfer =>
				n_ctr_led     <= x"00";
				n_dest_get    <= x"FF";
				n_clk_quarter <= 0;
				n_ctr_byte    <= 0;
				n_mosi        <= '1';
				n_ctr_bit     <= 0;
				n_sck         <= '1';
				if (start_transfer = '1') then
					n_state <= start_frame;
				else
					n_state <= wait_for_start_transfer;
				end if;

			when start_frame =>
				n_ctr_led  <= x"00";
				n_dest_get <= x"FF";
				n_mosi     <= '0';
				n_ctr_byte <= 0;
				n_ctr_led  <= x"00";
				case clk_quarter is
					when 0 =>
						n_sck <= '0';
					when 1 =>
						n_sck <= '0';
					when 2 =>
						n_sck <= '1';
					when 3 =>
						n_sck <= '1';
				end case;
				if (ctr_bit = 31 and clk_quarter = 3) then
					n_clk_quarter <= 0;
					n_ctr_bit     <= 0;
					n_state       <= transfer;
				elsif (clk_quarter = 3) then
					n_clk_quarter <= 0;
					n_ctr_bit     <= ctr_bit + 1;
					n_state       <= start_frame;
				else
					n_clk_quarter <= clk_quarter + 1;
					n_ctr_bit     <= ctr_bit;
					n_state       <= start_frame;
				end if;

			when transfer =>
				n_dest_get <= ctr_led;
				case ctr_byte is
					when 0 =>
						tmp    := std_logic_vector(byte0);
						n_mosi <= tmp(7 - ctr_bit);
					when 1 =>
						tmp    := std_logic_vector(data8_b_get);
						n_mosi <= tmp(7 - ctr_bit);
					when 2 =>
						tmp    := std_logic_vector(data8_g_get);
						n_mosi <= tmp(7 - ctr_bit);
					when 3 =>
						tmp    := std_logic_vector(data8_r_get);
						n_mosi <= tmp(7 - ctr_bit);
				end case;
				case clk_quarter is
					when 0 =>
						n_sck <= '0';
					when 1 =>
						n_sck <= '0';
					when 2 =>
						n_sck <= '1';
					when 3 =>
						n_sck <= '1';
				end case;
				if (clk_quarter = 3) then
					-- done with this bit
					n_clk_quarter <= 0;
					if (ctr_bit = 7) then
						-- done with this byte
						n_ctr_bit <= 0;
						if (ctr_byte = 3) then
							-- done with this led
							n_ctr_byte <= 0;
							if (ctr_led = num_leds - 1) then
								-- done with all the leds
								n_ctr_led <= x"00";
								n_state   <= end_frame;
							else
								-- more leds remaining
								n_ctr_led <= ctr_led + 1;
								n_state   <= transfer;
							end if;
						else
							-- more bytes remaining
							n_ctr_byte <= ctr_byte + 1;
							n_ctr_led  <= ctr_led;
							n_state    <= transfer;
						end if;
					else
						-- more bits remaining
						n_ctr_bit  <= ctr_bit + 1;
						n_ctr_byte <= ctr_byte;
						n_ctr_led  <= ctr_led;
						n_state    <= transfer;
					end if;
				else
					n_clk_quarter <= clk_quarter + 1;
					n_ctr_bit     <= ctr_bit;
					n_ctr_byte    <= ctr_byte;
					n_ctr_led     <= ctr_led;
					n_state       <= transfer;
				end if;

			when end_frame =>
				n_ctr_led  <= x"00";
				n_dest_get <= x"FF";
				n_mosi     <= '1';
				n_ctr_byte <= 0;
				n_ctr_led  <= x"00";
				case clk_quarter is
					when 0 =>
						n_sck <= '0';
					when 1 =>
						n_sck <= '0';
					when 2 =>
						n_sck <= '1';
					when 3 =>
						n_sck <= '1';
				end case;
				if (ctr_bit = 31 and clk_quarter = 3) then
					n_clk_quarter <= 0;
					n_ctr_bit     <= 0;
					n_state       <= idle;
				elsif (clk_quarter = 3) then
					n_clk_quarter <= 0;
					n_ctr_bit     <= ctr_bit + 1;
					n_state       <= end_frame;
				else
					n_clk_quarter <= clk_quarter + 1;
					n_ctr_bit     <= ctr_bit;
					n_state       <= end_frame;
				end if;

		end case;
	end process;
end behavioural;

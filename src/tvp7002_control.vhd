library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_misc.all;

entity tvp7002_control is
	port(
		rst      : in  std_logic;
		clk10khz : in  std_logic;
		resetb   : out std_logic
	);
end entity tvp7002_control;

architecture behavioural of tvp7002_control is

	signal cntr     : integer range 0 to 10001 := 0;
	signal n_cntr   : integer range 0 to 10001 := 0;
	signal n_resetb : std_logic                := '0';

begin
	process(clk10khz, rst)
	begin
		if (rising_edge(clk10khz)) then
			if (rst = '0') then
				resetb <= '0';
				cntr   <= 0;
			else
				resetb <= n_resetb;
				cntr   <= n_cntr;
			end if;
		end if;
	end process;

	process(clk10khz, cntr)
	begin
		if (cntr < 10000) then
			n_cntr   <= cntr + 1;
			n_resetb <= '0';
		else
			n_cntr   <= cntr;
			n_resetb <= '1';
		end if;
	end process;
end behavioural;

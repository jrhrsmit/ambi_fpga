library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_misc.all;

entity ambi_fpga_tb is
	generic(
		LEDS_H_MAX_IDX : integer := 30;
		LEDS_V_MAX_IDX : integer := 16;
		LINE_MAX_IDX   : integer := 1079;
		PX_MAX_IDX     : integer := 348
	);
end entity ambi_fpga_tb;

architecture behavioural of ambi_fpga_tb is
	signal clk27mhz        : std_logic                         := '0';
	signal rst             : std_logic                         := '0';
	signal vsync           : std_logic                         := '0';
	signal data_enable_syn : std_logic                         := '0';
	signal de_out          : std_logic                         := '0';
	signal led_x           : integer range 0 to LEDS_H_MAX_IDX := 0;
	signal led_y           : integer range 0 to LEDS_V_MAX_IDX := 0;
	signal r_in            : unsigned(7 downto 0)              := x"00";
	signal g_in            : unsigned(7 downto 0)              := x"00";
	signal b_in            : unsigned(7 downto 0)              := x"00";
	signal n_r_in          : unsigned(7 downto 0)              := x"00";
	signal n_g_in          : unsigned(7 downto 0)              := x"00";
	signal n_b_in          : unsigned(7 downto 0)              := x"00";
	signal r_out           : unsigned(7 downto 0)              := x"00";
	signal g_out           : unsigned(7 downto 0)              := x"00";
	signal b_out           : unsigned(7 downto 0)              := x"00";
	signal led_y_end       : std_logic                         := '0';
	signal data8_r         : unsigned(7 downto 0);
	signal data8_g         : unsigned(7 downto 0);
	signal data8_b         : unsigned(7 downto 0);
	signal dest_set        : unsigned(7 downto 0);
	signal data8_r_get     : unsigned(7 downto 0);
	signal data8_g_get     : unsigned(7 downto 0);
	signal data8_b_get     : unsigned(7 downto 0);
	signal dest_get        : unsigned(7 downto 0);

	component spi is
		port(
			clk            : in  std_logic;
			rst            : in  std_logic;
			sck            : out std_logic;
			mosi           : out std_logic;
			data8_r_get    : in  unsigned(7 downto 0);
			data8_g_get    : in  unsigned(7 downto 0);
			data8_b_get    : in  unsigned(7 downto 0);
			dest_get       : out unsigned(7 downto 0);
			start_transfer : in  std_logic;
			dip_sw         : in  std_logic_vector(7 downto 0)
		);
	end component spi;

	component led_pointer is
		generic(
			LEDS_H_MAX_IDX : integer := 30;
			LEDS_V_MAX_IDX : integer := 16;
			LINE_MAX_IDX   : integer := 1079;
			PX_MAX_IDX     : integer := 347
		);
		port(
			clk             : in  std_logic;
			rst             : in  std_logic;
			vsync           : in  std_logic;
			data_enable     : in  std_logic;
			data_enable_out : out std_logic;
			r_in            : in  unsigned(7 downto 0);
			g_in            : in  unsigned(7 downto 0);
			b_in            : in  unsigned(7 downto 0);
			r_out           : out unsigned(7 downto 0);
			g_out           : out unsigned(7 downto 0);
			b_out           : out unsigned(7 downto 0);
			led_x           : out integer range 0 to LEDS_H_MAX_IDX;
			led_y           : out integer range 0 to LEDS_V_MAX_IDX;
			led_y_end       : out std_logic
		);
	end component led_pointer;

	component vga is
		generic(
			LEDS_H_MAX_IDX : integer := 30;
			LEDS_V_MAX_IDX : integer := 16;
			LINE_MAX_IDX   : integer := 1079;
			PX_MAX_IDX     : integer := 348
		);
		port(
			clk            : in  std_logic;
			rst            : in  std_logic;
			vsync          : in  std_logic;
			data_enable    : in  std_logic;
			r              : in  unsigned(7 downto 0);
			g              : in  unsigned(7 downto 0);
			b              : in  unsigned(7 downto 0);
			led_x          : in  integer range 0 to LEDS_H_MAX_IDX;
			led_y          : in  integer range 0 to LEDS_V_MAX_IDX;
			led_y_end      : in  std_logic;
			r_avg          : out unsigned(7 downto 0);
			g_avg          : out unsigned(7 downto 0);
			b_avg          : out unsigned(7 downto 0);
			dest_set       : out unsigned(7 downto 0);
			dest_set_delay : out unsigned(7 downto 0);
			dip_sw         : in  std_logic_vector(7 downto 0);
			ss_debug_out   : out std_logic_vector(31 downto 0)
		);
	end component vga;

	component rgb_buffer is
		port(
			clk         : in  std_logic;
			rst         : in  std_logic;
			data8_r_set : in  unsigned(7 downto 0);
			data8_g_set : in  unsigned(7 downto 0);
			data8_b_set : in  unsigned(7 downto 0);
			dest_set    : in  unsigned(7 downto 0);
			data8_r_get : out unsigned(7 downto 0);
			data8_g_get : out unsigned(7 downto 0);
			data8_b_get : out unsigned(7 downto 0);
			dest_get    : in  unsigned(7 downto 0)
		);
	end component rgb_buffer;

begin

	clk27mhz <= not clk27mhz after 18.5185 ns;
	rst      <= '1' after 100 ns;

	rgb : process
	begin
		wait until rising_edge(data_enable_syn);
		r_in <= x"FF";
		g_in <= x"FF";
		b_in <= x"FF";
		wait for 4 us;
		r_in <= x"00";
		g_in <= x"00";
		b_in <= x"00";

	end process;

	vsync_process : process
	begin
		loop
			vsync <= '0';
			wait for 34 us;
			vsync <= '1';
			wait for 16632 us;
		end loop;
	end process;

	data_enable_process : process
	begin
		loop
			data_enable_syn <= '0';
			wait for 34 us;
			wait for 532 us;
			for I in 0 to 1079 loop
				data_enable_syn <= '1';
				wait for 12.929 us;
				data_enable_syn <= '0';
				wait for 1.9 us;
			end loop;
			wait for 72.8 us;
		end loop;
	end process;

	led_pointer_inst : led_pointer
		port map(
			clk             => clk27mhz,
			rst             => rst,
			vsync           => vsync,
			data_enable     => data_enable_syn,
			data_enable_out => de_out,
			led_x           => led_x,
			led_y           => led_y,
			led_y_end       => led_y_end,
			r_in            => r_in,
			g_in            => g_in,
			b_in            => b_in,
			r_out           => r_out,
			g_out           => g_out,
			b_out           => b_out
		);

	vga_inst : vga
		port map(
			clk         => clk27mhz,
			rst         => rst,
			vsync       => vsync,
			data_enable => de_out,
			r           => r_out,
			g           => g_out,
			b           => b_out,
			led_x       => led_x,
			led_y       => led_y,
			dip_sw      => x"04",
			led_y_end   => led_y_end,
			r_avg       => data8_r,
			g_avg       => data8_g,
			b_avg       => data8_b,
			dest_set    => dest_set
		);

	spi_inst : spi
		port map(
			clk            => clk27mhz,
			rst            => rst,
			dest_get       => dest_get,
			data8_r_get    => data8_r_get,
			data8_g_get    => data8_g_get,
			data8_b_get    => data8_b_get,
			start_transfer => vsync,
			dip_sw         => x"FF"
		);

	rgb_buffer_inst : rgb_buffer
		port map(
			clk         => clk27mhz,
			rst         => rst,
			data8_r_set => data8_r,
			data8_g_set => data8_g,
			data8_b_set => data8_b,
			dest_set    => dest_set,
			data8_r_get => data8_r_get,
			data8_g_get => data8_g_get,
			data8_b_get => data8_b_get,
			dest_get    => dest_get
		);
end;

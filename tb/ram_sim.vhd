library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_misc.all;

entity ram_sim is
	PORT(
		address : IN  STD_LOGIC_VECTOR(6 DOWNTO 0);
		clock   : IN  STD_LOGIC := '1';
		data    : IN  STD_LOGIC_VECTOR(23 DOWNTO 0);
		rden    : IN  STD_LOGIC := '1';
		wren    : IN  STD_LOGIC;
		q       : OUT STD_LOGIC_VECTOR(23 DOWNTO 0)
	);
end entity ram_sim;

architecture behavioural of ram_sim is
	type ram128x24 is array (0 to 127) of std_logic_vector(23 downto 0);
	signal ram : ram128x24 := (
		x"FF2100",
		x"FF4300",
		x"FF6600",
		x"FF8800",
		x"FFAA00",
		x"FFCC00",
		x"FFEE00",
		x"EEFF00",
		x"CCFF00",
		x"AAFF00",
		x"88FF00",
		x"65FF00",
		x"43FF00",
		x"21FF00",
		x"00FF00",
		x"00FF21",
		x"00FF43",
		x"00FF65",
		x"00FF87",
		x"00FFA9",
		x"00FFCB",
		x"00FFED",
		x"00EDFF",
		x"00CBFF",
		x"00A9FF",
		x"0087FF",
		x"0065FF",
		x"0043FF",
		x"0021FF",
		x"0000FF",
		x"2200FF",
		x"4300FF",
		x"6600FF",
		x"8700FF",
		x"AA00FF",
		x"CB00FF",
		x"EE00FF",
		x"FF00EE",
		x"FF00CB",
		x"FF00AA",
		x"FF0087",
		x"FF0066",
		x"FF0043",
		x"FF0022",
		x"FF0000",
		x"FF2100",
		x"FF4300",
		x"FF6600",
		x"FF8800",
		x"FFAA00",
		x"FFCC00",
		x"FFEE00",
		x"EEFF00",
		x"CCFF00",
		x"AAFF00",
		x"88FF00",
		x"65FF00",
		x"43FF00",
		x"21FF00",
		x"00FF00",
		x"00FF21",
		x"00FF43",
		x"00FF65",
		x"00FF87",
		x"00FFA9",
		x"00FFCB",
		x"00FFED",
		x"00EDFF",
		x"00CBFF",
		x"00A9FF",
		x"0087FF",
		x"0065FF",
		x"0043FF",
		x"0021FF",
		x"0000FF",
		x"2200FF",
		x"4300FF",
		x"6600FF",
		x"8700FF",
		x"AA00FF",
		x"CB00FF",
		x"EE00FF",
		x"FF00EE",
		x"FF00CB",
		x"FF00AA",
		x"FF0087",
		x"FF0066",
		x"FF0043",
		x"FF0022",
		x"FF0000",
		x"FF2100",
		x"FF4300",
		x"FF6600",
		x"FF8800",
		x"FFAA00",
		x"FFCC00",
		others => x"999999"
	);
	--	signal data_in : unsigned(31 downto 0) := (others => '0');
begin
	process(address, clock, data, rden, wren)
	begin
		if (rising_edge(clock)) then
			if (wren = '1') then
				ram(to_integer(unsigned(address))) <= data;
			end if;
			if (rden = '1') then
				q <= ram(to_integer(unsigned(address)));
			else
				q <= (others => 'X');
			end if;
		end if;
	end process;

end behavioural;

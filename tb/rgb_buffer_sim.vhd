library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_misc.all;

entity rgb_buffer is
	port(
		clk         : in  std_logic;
		rst         : in  std_logic;
		data8_r_set : in  unsigned(7 downto 0);
		data8_g_set : in  unsigned(7 downto 0);
		data8_b_set : in  unsigned(7 downto 0);
		dest_set    : in  unsigned(7 downto 0);
		data8_r_get : out unsigned(7 downto 0);
		data8_g_get : out unsigned(7 downto 0);
		data8_b_get : out unsigned(7 downto 0);
		dest_get    : in  unsigned(7 downto 0)
	);
end entity rgb_buffer;

architecture behavioural of rgb_buffer is
	signal addr         : std_logic_vector(6 downto 0)  := (others => '0');
	signal data_in      : std_logic_vector(23 downto 0) := (others => '0');
	signal wren         : std_logic                     := '0';
	signal rden         : std_logic                     := '0';
	signal data_out     : std_logic_vector(23 downto 0) := (others => '0');
	signal dest_set_tmp : std_logic_vector(7 downto 0)  := x"FF";
	signal dest_get_tmp : std_logic_vector(7 downto 0)  := x"FF";

	component ram_sim
		PORT(
			address : IN  STD_LOGIC_VECTOR(6 DOWNTO 0);
			clock   : IN  STD_LOGIC := '1';
			data    : IN  STD_LOGIC_VECTOR(23 DOWNTO 0);
			rden    : IN  STD_LOGIC := '1';
			wren    : IN  STD_LOGIC;
			q       : OUT STD_LOGIC_VECTOR(23 DOWNTO 0)
		);
	end component;

	--	signal data_in : unsigned(31 downto 0) := (others => '0');
begin

	data8_r_get           <= unsigned(data_out(23 downto 16));
	data8_g_get           <= unsigned(data_out(15 downto 8));
	data8_b_get           <= unsigned(data_out(7 downto 0));
	data_in(23 downto 16) <= std_logic_vector(data8_r_set);
	data_in(15 downto 8)  <= std_logic_vector(data8_g_set);
	data_in(7 downto 0)   <= std_logic_vector(data8_b_set);
	dest_set_tmp          <= std_logic_vector(dest_set);
	dest_get_tmp          <= std_logic_vector(dest_get);
	wren                  <= not dest_set_tmp(7);
	rden                  <= dest_set_tmp(7);

	process(dest_set_tmp, dest_get_tmp, wren, rden)
	begin
		for I in 0 to 6 loop
			addr(I) <= (dest_set_tmp(I) and wren) or (dest_get_tmp(I) and rden);
		end loop;
	end process;

	ram_inst : ram_sim
		port map(
			address => addr,
			clock   => clk,
			data    => data_in,
			rden    => rden,
			wren    => wren,
			q       => data_out
		);

end behavioural;
